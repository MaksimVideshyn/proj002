package com.example.utils;

public interface Const {
    int PORT = 8080;
    String PONG = "pong";
    String PING = "ping";
    String LOCAL_HOST = "localhost";

    //SQL
    String SELECT_VERSION = "SELECT version()";
    String INSERT_INFO = "INSERT INTO proj001(res_body, created_at) VALUES(?, ?)";

    //batis
    String INSERT_METHOD = "insertMessage";
    String ENVIRONMENT_ID = "development";
}
