package com.example.batis;

import com.example.db.model.Message;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

import static com.example.utils.Const.ENVIRONMENT_ID;
import static com.example.utils.Const.INSERT_METHOD;

@Component
public class BatisHelper {

    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public BatisHelper(DataSource dataSource) {
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment(ENVIRONMENT_ID, transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);

        configuration.addMapper(MessageMapper.class);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
    }

    public void insertMessage(final String msg) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            final Message message = new Message(msg);
            session.update(INSERT_METHOD, message);
            session.commit();
        }
    }

}
