package com.example;

import com.example.client.Client;
import com.example.server.NettyServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        final ApplicationContext context = SpringApplication.run(Main.class, args);
        final NettyServer nettyServer = context.getBean(NettyServer.class);
        final Client client = new Client();
        nettyServer.setListener(client::createSocketClient);
        try {
            nettyServer.startNettyServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
