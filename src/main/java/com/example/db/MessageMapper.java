package com.example.db;

import com.example.db.model.Message;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageMapper implements RowMapper<Message> {

    @Override
    public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
        final Message message = new Message();
        message.setId(rs.getInt("id"));
        message.setMsg(rs.getString("res_body"));
        message.setTimestamp(rs.getTimestamp("created_at"));
        return message;
    }
}
