package com.example.db.dao;

import com.example.db.model.Message;

import java.util.List;

public interface MessageDao {

    Message get(Integer id);
    void create(Message message);
    void delete(Integer id);
    List<Message> getList();

}
