package com.example.db.dao;

import java.sql.Connection;

public interface DaoFactory {

    Connection getConnection();
    MessageDao getMessageDao();

}
