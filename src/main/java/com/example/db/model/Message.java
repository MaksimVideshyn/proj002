package com.example.db.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

public class Message {

    private Integer id;
    private String msg;
    private Timestamp timestamp;

    public Message() {
    }

    public Message(String msg) {
        this.msg = msg;
        final Calendar utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        timestamp = new Timestamp(utc.getTimeInMillis());
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Message{" +
                "msg='" + msg + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
