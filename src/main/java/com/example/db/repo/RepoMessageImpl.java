package com.example.db.repo;

import com.example.db.MessageMapper;
import com.example.db.model.Message;
import com.example.db.repo.interfaces.Repository;
import com.example.db.repo.interfaces.Specification;
import com.example.db.repo.interfaces.SqlSpecification;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class RepoMessageImpl implements Repository<Message> {

    private final JdbcTemplate jdbcTemplate;

    public RepoMessageImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void addMessage(Message message) {
        final String sql = "INSERT INTO proj001(res_body, created_at) VALUES (?, ?)";
        jdbcTemplate.update(sql, message.getMsg(), message.getTimestamp());
    }

    @Override
    public void removeMessage(Message message) {
        final String sql = "DELETE FROM proj001 WHERE id = ?";
        jdbcTemplate.update(sql, message.getId());
    }

    @Override
    public void updateMessage(Message message) {

    }

    @Override
    public List<Message> query(Specification specification) {
        return jdbcTemplate.query(((SqlSpecification) specification).toSqlQuery(), new MessageMapper());
    }
}
