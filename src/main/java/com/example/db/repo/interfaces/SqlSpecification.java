package com.example.db.repo.interfaces;

public interface SqlSpecification extends Specification {
    String toSqlQuery();
}
