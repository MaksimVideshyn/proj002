package com.example.db.repo.interfaces;

import java.util.List;

public interface Repository<T> {
    void addMessage(T message);
    void removeMessage(T message);
    void updateMessage(T message);
    List<T> query(Specification specification);
}
