package com.example.db.repo.specifications;

import com.example.db.model.Message;
import com.example.db.repo.interfaces.SqlSpecification;

public class MessagesByResBodySpecification implements SqlSpecification {

    private final Message message;

    public MessagesByResBodySpecification(Message message) {
        this.message = message;
    }

    @Override
    public String toSqlQuery() {
        return String.format(
                "SELECT * FROM proj001 WHERE res_body = '%s'",
                message.getMsg()
        );
    }
}
