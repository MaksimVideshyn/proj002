package com.example.db.daoImpl;

import com.example.db.MessageMapper;
import com.example.db.dao.MessageDao;
import com.example.db.model.Message;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class MessageDaoImpl implements MessageDao {

    private JdbcTemplate jdbcTemplate;

    public MessageDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Message get(Integer id) {
        final String sql = "SELECT * FROM proj001 WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new MessageMapper());
    }

    @Override
    public void create(Message message) {
        final String sql = "INSERT INTO proj001(res_body, created_at) VALUES (?, ?)";
        jdbcTemplate.update(sql, message.getMsg(), message.getTimestamp());
    }

    @Override
    public void delete(Integer id) {
        final String sql = "DELETE FROM proj001 WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public List<Message> getList() {
        final String sql = "SELECT * FROM proj001";
        return jdbcTemplate.query(sql, new MessageMapper());
    }
}
