package com.example.server;

import com.example.batis.BatisHelper;
import com.example.db.daoImpl.MessageDaoImpl;
import com.example.db.model.Message;
import com.example.db.repo.RepoMessageImpl;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.nio.charset.Charset;

import static com.example.utils.Const.PONG;
import static com.example.utils.Const.PORT;

@Component
public class NettyServer {

    private final JdbcTemplate jdbcTemplate;
    private final DataSource dataSource;
    private Listener listener;

    @Autowired
    public NettyServer(JdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    public void startNettyServer() throws Exception {
        final EventLoopGroup bossGroup = new NioEventLoopGroup();
        final EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            final ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(
                                    new StringEncoder(Charset.forName("UTF8")),
                                    new StringDecoder(),
                                    new LineBasedFrameDecoder(1024),
                                    new ServerHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            final ChannelFuture future = bootstrap.bind(PORT).sync();

            System.out.println("DiscardServer, Open Server");

            if (listener != null) listener.readyToConnect();

            future.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void readyToConnect();
    }

    class ServerHandler extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("DiscardServerHandler, msg = " + msg);

//            final RepoMessageImpl repoMessage = new RepoMessageImpl(jdbcTemplate);
//            repoMessage.addMessage(new Message(msg.toString()));

//            final MessageDaoImpl messageDao = new MessageDaoImpl(jdbcTemplate);
//            messageDao.create(new Message(msg.toString()));

            final BatisHelper batisHelper = new BatisHelper(dataSource);
            batisHelper.insertMessage(msg.toString());

            ctx.channel().writeAndFlush(PONG);

        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
            cause.printStackTrace();
            ctx.close();
        }

    }

}
